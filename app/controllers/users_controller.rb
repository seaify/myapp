class UsersController < ApplicationController

  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json do
        @users = User.where("name LIKE ? OR email LIKE ? OR phone LIKE ?", "%#{params[:keyword]}", "%#{params[:keyword]}", "%#{params[:keyword]}")
        render :json => {data: @users.order(params[:sortField] + ' ' + params[:sortDirection]).page(params[:pageNumber]).per(params[:pageSize]), total: @users.count, totalPages: (@users.count / params[:pageSize].to_f).ceil}
      end
    end
  end

  def show

  end
end
