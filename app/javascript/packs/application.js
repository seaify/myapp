/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

import Vue from 'vue'
import UserList from './user/list.vue'

document.addEventListener('DOMContentLoaded', () => {
  if (document.querySelector('#user_list')) {
    document.body.appendChild(document.createElement('hello'))
    const app = new Vue({
      template: `<UserList apiUrl="/users.json" :fields="['id','name','email','phone']" />`,
      components: { UserList }
    }).$mount('hello')
    console.log(app)
  }
})