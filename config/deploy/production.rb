set :unicorn_config, -> { "#{deploy_to}/#{current_path}/config/unicorn/production.rb" }
set :branch, 'master'
set :rails_env, 'production'

set :domain, '118.193.169.183'
set :deploy_to, '/home/deploy/myapp'
set :repository, 'git@bitbucket.org:seaify/myapp.git'
set :user, 'deploy'
