set :branch, 'staging'
set :unicorn_config, -> { "#{deploy_to}/#{current_path}/config/unicorn/staging.rb" }
set :unicorn_pid, -> { "#{deploy_to}/#{current_path}/tmp/pids/unicorn-staging.pid" }
set :rails_env, 'staging'

#set variables
set :domain, '118.193.169.183'
set :deploy_to, '/home/deploy/myapp_staging'
set :repository, 'git@bitbucket.org:seaify/myapp.git'
set :user, 'deploy'
