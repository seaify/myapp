require 'json'
require 'faker'

result =  []
50.times do
  result.push({name: Faker::Name.first_name, email: Faker::Internet.email, company: Faker::Company.name})
end

File.open('data.json', 'w') do |f|
  f.puts result.to_json
end